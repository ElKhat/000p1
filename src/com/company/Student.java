package com.company;

public class Student extends Person implements Politeness {

    public Student(){}

    public Student(int age, String name, String lastName){
        setAge(age);
        setName(name);
        setLastname(lastName);
    };

    public void  goToShcool(){
        System.out.println("Я иду в школу");
    }


    @Override
    public void sayGoodBye() {
        System.out.println("Buy from Student");
    }

    @Override
    public void calculateAge() {
        System.out.println(getAge()-10);
    }

    @Override
    public String addPrefix(String prefixName) {
        String subName = prefixName+" " + getName() +" "+ getLastname();
        return subName;
    }
}

