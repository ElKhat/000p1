package com.company;

public class Teacher extends Person implements Politeness{

    public Teacher(int age, String name, String lastName){
        setAge(age);
        setName(name);
        setLastname(lastName);
    }

    public void goToWork(){
        System.out.println(" иду на работу");
    }

    @Override
    public void sayGoodBye() {
        System.out.println( "Buy from teacher");
    }

    @Override
    public void calculateAge() {
        System.out.println(getAge()*10);
    }

    @Override
    public String addPrefix(String prefixName) {
        String someName  = getName() + "" + getLastname() +" " + prefixName;
        return someName;
    }
}
