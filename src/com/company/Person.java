package com.company;

public class Person {
    private String name;
    private  String lastname;
    private  int age;

    public void  showInfo(){
        System.out.println("  Name:"+ name+"\n  Last Name:" + lastname+"\n  Age:" + String.valueOf(age));
    }



    public void sayHello(){
        System.out.println("Привет");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }


}
